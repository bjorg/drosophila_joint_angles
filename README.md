# drosophila_joint_angles

A repository to keep the algorithm to calculate joint angles from leg
joint positions of Drosophila melanogaster and feed them to a Drosophila
skeletal model in Webots. The world, the calculator and the kinematic
controller are all found here.

### Folder structure:
* *Data* - here the 3D position data of the Drosophila should be kept
           as .npy files. The structure of the data must be
           [time,[leg 1 point 1, leg 1 point 2, etc.], [x,y,z]]

* *drosophila_joint_angles* - here the functions for computing the 
                              joint angles are kept. The functions
                              developed specifically for calculating
                              joint angles are in the file
                              joint_functions.py. Rotation matrix
                              transformations used in the project are
                              found in transformations.py (see copyright
                              notice at the start of this file).

* *webots* - this folder holds all files necessary to run the Webots
             world and kinematic controller. The controllers folder
             includes the kinematic controller, the worlds folder has
             the Webots world and the plugins folder contains the
             necessary physics to run the simulation. The
             Drosophila_eye_texture.png is a picture to give the eyes of
             the Webots fly it's red fly-like look and is there for
             purely aesthetic reasons.

### Dependencies
The kinematic controller runs *Python 2.7+*. The packages used are the following:
* functools
* scipy
* numpy
* pandas
* matplotlib
* future

 The *Webots* version used during development and testing was Webots R2018b on Ubuntu 18.04.

### Author information

*Author*: Halla Bjorg Sigurthorsdottir

A part of the work for the master thesis project in Life Sciences and Technologies at EPFL 2018-2019.

*Supervisors:* Professor Pavan Ramdya and Professor Auke Ijspeert