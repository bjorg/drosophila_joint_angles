#!/usr/bin/env python

# File:          drosophila_kinematic_analysis.py
# Date:          Winter 2018
# Description:   Skeletal model of the drosophila model to replicate
# kinematics recorded from real fly experiments
# Authors:        Shravan Tata Ramalingasetty
#                 Halla Bjorg Sigurthorsdottir

# Add/remove the paths (Webots starts 'python2.7' when it runs)

# Import packages
import os
import sys
import pandas as pd
import numpy as np

wd = os.getcwd()
repo_dir = os.getcwd()[:wd.find('drosophila_joint_angles')]
sys.path.append(repo_dir + 'drosophila_joint_angles')

from drosophila_joint_angles import joint_functions as jf 
from controller import Motor, Supervisor

class Drosophila(Supervisor):
    """Main Class for Drosophila Model Kinematic matching.
    """

    def __init__(self, input_var):
        super(Drosophila, self).__init__()
        #: container to store kinematics
        self.kinematics = None

        #: container to store the webots motor instances
        self.motors = {}

        #: Initialize time
        self.time = 0.0

        #: Step the kinematic counter
        self.kinematic_step = 0

        #: container to store the webots motor position instances
        self.position_sensors = {}

        #: get the time step of the current world.
        self.time_step = int(self.getBasicTimeStep())
        self.read_kinematic_data(input_var)
        self.generate_position_sensors()
        self.generate_motors()

    def read_kinematic_data(self, input_var):
        """Read the kinematics file.
        Parameters
        ----------
        input_var: <list> [path, input_fps, export_name]
            path: File path location containing the .npy file containing joint positions.
            input_fps: The frames per second of the captured data.
            export_name: (optional) The file path and file name of the file where the joint angles should be saved.
        """
        
        path = input_var[1]
        input_fps = float(input_var[2])
        path = os.path.join(repo_dir, 'drosophila_joint_angles', 'Data', path) #abspath[:-50] + path

        if len(input_var) > 3: # If
            export_name = input_var[3]
        else:
            export_name = None

        if not os.path.exists(path):
            raise Exception('File not found : {}'.format(path))
        else:
            if os.path.splitext(path)[-1].lower() == '.npy':
                if not os.path.splitext(path)[-1].lower() == '.npy':
                    raise Exception(
                        'File extension not supported : *.npy needed but found {}'.format(
                            path))
                self.kinematics = jf.get_joint_angles(path, time_step = 1000/input_fps, joint_angle_filename = export_name)
            else:
                if not os.path.splitext(path)[-1].lower() == '.csv':
                    raise Exception(
                        'File extension not supported : *.csv needed but found {}'.format(
                            path))
                self.kinematics = pd.read_csv(path)
            
            
            
    def generate_motors(self):
        """Initialize the webots motors."""
        #: pylint: disable=no-member
        for joint in self.kinematics.keys():
            _motor = self.getMotor(str(joint) + '_ROT')
            if _motor is not None:
                self.motors[joint] = _motor
                print('Created motor for joint : {}'.format(
                    joint))
            else:
                print('Joint -> {} not found'.format(joint))

        self.head = self.getMotor('THORAX_HEAD_T_2_ROT')
        antenna_joints = ['L_HEAD_SCAPE', 'L_SCAPE_FUNICULUS', 'R_HEAD_SCAPE', 'R_SCAPE_FUNICULUS']
        self.antennas = {}
        for joint in antenna_joints:
            _motor = self.getMotor(str(joint) + '_ROT')
            if _motor is not None:
                self.antennas[joint] = _motor
                print('Created motor for joint : {}'.format(
                    joint))
            else:
                print('Joint -> {} not found'.format(joint))


    def generate_position_sensors(self):
        """Initialize webots position sensor."""
        #: pylint: disable=no-member
        for joint in self.kinematics.keys():
            _pos_sensor = self.getPositionSensor(str(joint) + '_POS')
            if _pos_sensor is not None:
                self.position_sensors[joint] = _pos_sensor
                self.position_sensors[joint].enable(self.time_step)
                print('Created position sensor for joint : {}'.format(
                    joint))
            else:
                print(
                    'Position sensor for joint -> {} not found'.format(joint))

    def update(self):
        self.head.setPosition = 0
        for name, joint in self.antennas.iteritems():
            joint.setPosition(0)

        for name, joint in self.motors.iteritems(): 
            try: 
                joint.setPosition(
                    self.kinematics[name][self.kinematic_step])
                out = False
            except KeyError:
                out = False
                if self.time > self.kinematics['TIME'].iloc[-1]:
                    print('End of simulation.....')
                    out = True

        #: Update the kinematic index
        self.kinematic_step += 1
        return out

    def run(self):
        """Main loop for running the mode """
        while self.step(self.time_step) != -1:
            #: Update time
            self.time += self.time_step
            out = self.update()
            if out:
                break


if __name__ == '__main__':
    controller = Drosophila(sys.argv)
    
    controller.run()
