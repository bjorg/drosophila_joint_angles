/*
 * File:         contact_points_physics.c
 * Date:         November 6, 2009
 * Description:  Example of use of a physics plugin to get information on
 *               the contact points and ground reaction forces of an object
 *               that collides with the floor
 * Author:       Yvan Bourquin
 *
 * Copyright (c) 2009 Cyberbotics - www.cyberbotics.com
 */

#include <ode/ode.h>
#include <plugins/physics.h>
#include <string.h>

#define MAX_CONTACTS 20
#define DRAW_CONTACT 1

// plugin variables
static dWorldID world = NULL;
static dSpaceID space = NULL;
static dJointGroupID contact_joint_group = NULL;

static dGeomID claw_geom[6];
static dGeomID thorax_geom;
static dBodyID claw_body[6];
static dBodyID thorax_body;

static dJointID contact_joints[MAX_CONTACTS];
static dGeomID floor_geom = NULL;

static dContact contacts[MAX_CONTACTS];

static int nContacts_claw[6] = {0,0,0,0,0,0};
static dJointFeedback feedbacks[MAX_CONTACTS];
static int first_contact = 0;

static int counter = 0;

// Cow adhesion coefficient ( 0: no adhesion, 1: 100% body weght)
static float adhesion_coefficient = 2;

static float fx = 0;
static float fy = 0;
static float fz = 0;

static int nContacts_sphere = 0;
FILE* tracker_file = NULL;
FILE* force_file = NULL;

char filename[200];

// plugin function called by Webots at the beginning of the simulation
void webots_physics_init(dWorldID w, dSpaceID s, dJointGroupID j) {

  // store global objects for later use
  world = w;
  space = s;
  contact_joint_group = j;

  // get ODE handles to .wbt objects

  
  floor_geom = dWebotsGetGeomFromDEF("FLOOR");

  thorax_geom = dWebotsGetGeomFromDEF("THORAX");


  claw_geom[0] = dWebotsGetGeomFromDEF("RF_CLAW");
  claw_geom[1] = dWebotsGetGeomFromDEF("RM_CLAW");
  claw_geom[2] = dWebotsGetGeomFromDEF("RH_CLAW");
  claw_geom[3] = dWebotsGetGeomFromDEF("LH_CLAW");
  claw_geom[4] = dWebotsGetGeomFromDEF("LM_CLAW");
  claw_geom[5] = dWebotsGetGeomFromDEF("LF_CLAW");

  
  // get claws' and thorax bodies (the floor does not have a body)
  thorax_body = dGeomGetBody(thorax_geom);

  claw_body[0] = dGeomGetBody(claw_geom[0]);
  claw_body[1] = dGeomGetBody(claw_geom[1]);
  claw_body[2] = dGeomGetBody(claw_geom[2]);
  claw_body[3] = dGeomGetBody(claw_geom[3]);
  claw_body[4] = dGeomGetBody(claw_geom[4]);
  claw_body[5] = dGeomGetBody(claw_geom[5]);
  
  // char filenamePath[200],numChar[50];
  // int number=1;
    // strcpy(filenamePath, "/home/thandiac/Documents/EPFL/Projects/16_Drosophila/webots_unit_resizing/controllers/drosophila/tracker_595_21_0_mu4_");
  // strcpy(filenamePath, "/data/thandiac/Drosophila/data_logging/tracker_595_21_0_mu4_");
  // strcpy(filename,filenamePath);
  // sprintf(numChar,"%d.csv",number);
  // strcat(filename,numChar);
  // while(fopen(filename,"r"))
  // {
  //  number++;
  //  strcpy(filename,filenamePath);
  //  sprintf(numChar,"%d.csv",number);
  //  strcat(filename,numChar);
  // } 
}

// plugin function called by Webots for every WorldInfo.basicTimeStep
void webots_physics_step() {
  // nothing to do (in this example)
    

     counter++;

    //sets the gravity orientation if the fly is stable ("landed" on 6 contact points)
     if((counter > 250) && (first_contact == 0))

     {

      dWorldSetGravity(world,0, -98.10, 0);     
      first_contact = 1;
      dWebotsConsolePrintf("Gravity set \n");

     }

}

// it is not really necessary but it's fun to draw the contact points
void webots_physics_draw(int pass, const char *view) {

  if(DRAW_CONTACT)
  {
    if (pass != 1)
      return;
    
    // change OpenGL state
    glDisable(GL_LIGHTING);    // not necessary
    glLineWidth(10);           // use a thick line
    glDisable(GL_DEPTH_TEST);  // draw in front of Webots graphics


    int i;
    for (i = 0; i < 6; i++) {
      dReal *p = contacts[i].geom.pos;
      dReal *n = contacts[i].geom.normal;
      dReal d = contacts[i].geom.depth * 30;
      glBegin(GL_LINES);
      if(i==0 || i ==2 || i ==4)
        glColor3f(1, 0, 0);  // draw in red
      else
        glColor3f(0, 1, 0);  // draw in red
      glVertex3f(p[0], p[1], p[2]);
      glVertex3f(p[0] + n[0]*0.1, p[1] + n[1]*0.1, p[2] + n[2]*0.1);
      glEnd();
    }

  }
}

// This function is implemented to overide Webots collision detection.
// It returns 1 if a specific collision is handled, and 0 otherwise. 
int webots_physics_collide(dGeomID g1, dGeomID g2) {

  int k = 0;
  
  nContacts_sphere =0;
  

  for(k=0 ; k<6 ; k++)
      {
        // check if this collision involves the objects which interest us
        if (((g1 == claw_geom[k] && g2 == floor_geom) || (g2 == claw_geom[k] && g1 == floor_geom)))
        {
          world = dBodyGetWorld(dGeomGetBody(g1));
          contact_joint_group = dWebotsGetContactJointGroup();

          // see how many collision points there are between theses objects
          nContacts_claw[k] = dCollide(g1, g2, MAX_CONTACTS, &contacts[k].geom, sizeof(dContact));
          //dWebotsConsolePrintf("contacts claw %d = %d \n", k, nContacts_claw[k]);
          
          nContacts_sphere = nContacts_sphere + nContacts_claw[k];
         

          // custom parameters for creating the contact joint
          // remove or tune these contact parameters to suit your needs
          contacts[k].surface.mode = dContactBounce | dContactSoftCFM | dContactApprox1;
          contacts[k].surface.mu =4.0;//4.0; Coulomb friction coefficient. This must be in the range 0 to dInfinity. 0 results in a frictionless contact, and dInfinity results in a contact that never slips.
          //contacts[k].surface.bounce = 0.005; Restitution parameter (0..1). 0 means the surfaces are not bouncy at all, 1 is maximum bouncyness.
          //contacts[k].surface.bounce_vel = 0.1; The minimum incoming velocity necessary for bounce. Incoming velocities below this will effectively have a bounce parameter of 0.
          contacts[k].surface.soft_cfm = 1e-5;//0.001; softness or springiness, if CFM is set to zero ; the constraint will be hard ;if CFM is set to a positive value; it will be possible to violate the constraint by pushing on it

          // create a contact joint that will prevent the two bodies from intersecting
          // note that contact joints are added to the contact_joint_group
          contact_joints[k] = dJointCreateContact(world, contact_joint_group, &contacts[k]);


          // attach joint between the body and the static environment (0)
          dJointAttach(contact_joints[k], claw_body[k], 0);
          
          // attach feedback structure to measure the force on the contact joint
          dJointSetFeedback(contact_joints[k], &feedbacks[k]);

          fx = 0.0;
          fy = adhesion_coefficient*83.02203;
          fz = 0.0;
          

          if(nContacts_claw[k])
          {
            dBodyAddForce(claw_body[k], -fx, -fy, -fz);
          }

          return 1;  // collision was handled above

        }
      }
  


  return 0;  // collision must be handled by webots
}

// convenience function to print a 3d vector
void print_vec3(const char *msg, const dVector3 v) {
  dWebotsConsolePrintf("%s: %g %g %g\n", msg, v[0], v[1], v[2]);
}

// this function is called by Webots after dWorldStep()
void webots_physics_step_end() {
  // Step tracking: make sure the path is right.
  /*tracker_file = fopen(filename, "a+");
 
  if (tracker_file != NULL)
  {
    fprintf(tracker_file, "%d,%d,%d,%d,%d,%d,%f\n", nContacts_claw[0], nContacts_claw[1], nContacts_claw[2], nContacts_claw[3], nContacts_claw[4], nContacts_claw[5],dWebotsGetTime()*0.001); 
              
    fclose(tracker_file);
  }*/
  

  /*
   // Force tracking: make sure the path is right.
   force_file = fopen("/Applications/Webots_Drosophila/Droso_6.0_small_adhesion_PSO/controllers/drosophila/forces.csv", "a+");
   
   if (force_file != NULL)
   {
   fprintf(force_file, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", 
       feedbacks[0].f1[0],feedbacks[0].f1[1],feedbacks[0].f1[2], 
       feedbacks[0].t1[0],feedbacks[0].t1[1],feedbacks[0].t1[2], 
       feedbacks[1].f1[0],feedbacks[1].f1[1],feedbacks[1].f1[2], 
       feedbacks[1].t1[0],feedbacks[1].t1[1],feedbacks[1].t1[2], 
       feedbacks[2].f1[0],feedbacks[2].f1[1],feedbacks[2].f1[2], 
       feedbacks[2].t1[0],feedbacks[2].t1[1],feedbacks[2].t1[2],
       feedbacks[3].f1[0],feedbacks[3].f1[1],feedbacks[3].f1[2], 
       feedbacks[3].t1[0],feedbacks[3].t1[1],feedbacks[3].t1[2],
       feedbacks[4].f1[0],feedbacks[4].f1[1],feedbacks[4].f1[2], 
       feedbacks[4].t1[0],feedbacks[4].t1[1],feedbacks[4].t1[2],
       feedbacks[5].f1[0],feedbacks[5].f1[1],feedbacks[5].f1[2], 
       feedbacks[5].t1[0],feedbacks[5].t1[1],feedbacks[5].t1[2]);
   fclose(tracker_file);
   }
  */
  
  
  // Set all the contacts to 0
  int i = 0;
  for (i = 0; i < 6; i++)   
  {
 
    
    nContacts_claw[i] = 0;
    //printf force and torque that each contact joint
    //applies on the box's body
    //dWebotsConsolePrintf("CLAW contact : %d:\n", i);
    //print_vec3("f1", feedbacks[i].f1);
    //print_vec3("t1", feedbacks[i].t1);
    //dWebotsConsolePrintf("\n");
    
    
}

}

// this function is called by Webots to cleanup memory before unloading the physics plugin
void webots_physics_cleanup() {
  // nothing to cleanup (in this example)
}
