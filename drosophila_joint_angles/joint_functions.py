
'''
File:             joint_functions.py
Date:             Winter 2018 - 2019
Description:      A file that keeps functions used to compute the joint angles from
                  3D joint positions (inverse kinematics) recorded from real fly experiments
Authors:          Halla Bjorg Sigurthorsdottir
Python version:   Python 2.7+
'''

#: Import packages
import copy
import functools
import scipy.optimize
import numpy as np
from numpy.linalg import norm
import pandas as pd

#: For plotting
import matplotlib.pyplot as plt
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D

#: Import custom modules
import drosophila_joint_angles.transformations as T
from one_euro_filter import OneEuroFilter

#: The main joint angle calculator
def get_joint_angles(path,
                     fix_body_coxa=True, gravity_reference='y', forward_reference='HMcoxa',
                     convention='syzx', eul_coxa_order=[0, 2, 1], negative_angles=True,
                     negative_joints=['F_COXA_FEMUR'], coxa_x_ref=[3, 4],
                     ld_correction=np.pi/2, plot_large_difference=False, verify=False,
                     verify_plot=False, time_points_to_verify=None, rainbow_leg_plot=False,
                     coxa_frame_plot=False, coxa_frame_projection='3d', fly_frame_plot=False,
                     fly_frame_projection='3d', leg_plane_plot=False,
                     joint_names=['THORAX_COXA', 'COXA_FEMUR', 'FEMUR_TIBIA', 'TIBIA_TARSUS'],
                     coxa_names=['LD', 'T_1', 'T_2'], side_names=['R', 'L'],
                     leg_names=['F', 'M', 'H'], last_point_name='CLAW',
                     no_legs=6, points_per_leg=5,
                     interpolate=True, time_step=10, webots_factor=0.1,
                     webots_time_step=2, wt_get_to_position=1000,
                     joint_angle_filename=None, plane_dist_filename=None,
                     segment_length_filename=None, plane_angle_name=None, error_name=None
                    ):
    '''
    This function takes in the path to a file of 3D joint positions over time and returns the
    corresponding joint angles.
    The data should be on a .npy format with dimensions time x joints x [x, y, z].
    It is assumed that the six legs have 6 degrees of freedom, 3 on the coxa joint and 1 on
    the three others.
    Assumption: none of the leg joints will ever rotate a 360 degrees

    Parameters:
        path - this is either the path to the data or a 3D numpy array containing the data.
               The data must be of the form [time, [leg 1 point 1, leg 1 point 2, etc.], xyz].

        fix_body_coxa - If true, the body-coxa points are fixed to the median of their position
                        over time.

        gravity_reference - this variable defines the method of defining the gravity axis of the
                            fly. "y" uses the y-axis of the data (second axis), "coxa" uses the
                            normal to the body-coxa plane.

        forward_reference - this vairable defines the method of determining the direction of
                            forward of the fly. "x" uses the x-axis of the data (first axis)
                            projected onto the plane defined by the gravity axis [has gravity
                            axis as normal], "coxa" uses the difference between mean of the hind
                            body-coxa points and the mean of the front body-coxa points
                            projected onto the plane defined by the gravity axis, "HMcoxa" uses
                            the difference between the hind body-coxa point and middle body-coxa
                            point on each side projected onto the plane defined by the gravity axis.

        convention - the order of which the axes are considered when calculating Euler angles
                     exmple: 'sxyz'. Default is 'syzx'.

        eul_coxa_order - the axes of rotation of the names in coxa_names (according to the
                         convention specified in the parameter convention).

        negative_angles - True: allows 1DOF joint angles to be negative, False: ignores this
                          part of the algorithm.

        negative_joints - A list of 1DOF joints allowed to be negative (possible entries: all
                          1DOF joint names in joint_names OR those names with an entry from
                          leg_names before [example: "F_COXA_FEMUR" allows both front coxa
                          femur joints to be negative]).

        coxa_x_ref - the point/points that are used as reference to define the leg rotationa
                     round the coxa. It is either an int or a vector of translation from the
                     thorax-coxa joint (toward the claw).
                     1 - coxa-femur, 2 - femur-tibia, 3 - tibia-tarsus, 4 - claw.

        ld_correction - "large difference" correction. If this is not None, the value (float)
                        is used to correct the data if there are big jumps (big defined by
                        this value) in the joint angle in the same direction within the file.

        plot_large_difference - if this is True, a plot of the corrected files will be shown

        verify - whether to calculate error of the coxa-femur and femur-tibia positions

        verify_plot - whether to plot the reconstructed joint positions alongside the joint
                      positions from the original data

        time_points_to_verify - a list of time points to plot the verification plots of.
                                If this is None and verify_plot is True, the reconstruction
                                will be plotted for all time points.

        rainbow_leg_plot - whether to plot all legs of the first frame with different colors
                           per leg (this is to verify that the right leg is on the right etc.)

        coxa_frame_plot - whether to plot the coxa frame alongside the leg that the coxa frame
                          is being calculated for

        coxa_frame_projection - whether the coxa frame plot should have a 3d or 2d projection
                                (values: '3d' or '2d')

        fly_frame_plot - whether to plot the fly frame with respect to legs

        fly_frame_projection - whether the fly frame plot should have a 3d or 2d projection
                               (values: '3d' or '2d')

    Input data parameters:
        joint_names - these are the names of the joints, the order here determines the order
                      in the input file.

        coxa_names - these are the names of the extensions to the name of the coxa joint
                     (the 3DOF joint).
                     The order determines the order in the output file.

        side_names - the names of the sides (L and R). The order defines the order in the
                     input file.

        leg_names - the names of the legs (F, M, H). The order defines the order in the
                    input file.

        last_point_name - the name of the final point of each leg. Default is 'CLAW'.

        DOF_3_point_name - the name of the 3DOF joint. Default is 'COXA'.

        no_legs - the number of legs. Default is 6.

        points_per_leg - the number of datapoints per leg. Default is 5.

        time_step - time step between datapoints in real data (in ms). Default is 50.

    Output data parameters:
        webots_factor - the factor by which Webots multiplies time. Default is 0.1.

        webots_time_step - the time step in Webots (as it is inputted). Default is 2.

        wt_get_to_position - the time (in Webots time) needed to get safely into position.
                             Default is 1000.

        joint_angle_filename - the name of the csv file (including .csv extension) that the
                               joint angles should be exported to. Default value is None,
                               meaning the function does not export the joint angles as a .csv
                               file.

        plane_dist_filename - the name of the pickle file (including .p extnsion) that the
                              distances of the leg points to the leg plane are saved to.

        segment_length_filename - the name of the txt file (including .txt extension) keeping
                                  mean leg segment lengths of the fly.

        plane_angle_name - the name of the .p file (including .p extension) that keeps the
                           angle between the coxa-femur plane and coxa-[tibia-tarsus joint]
                           plane, coxa-femur plane and coxa-claw plane and
                           coxa-[tibia-tarsus joint] and coxa-claw plane (they are in this
                           order in the last coordinate of the 3D output array) over time
                           for each leg.
    '''

    #: Load the data
    print('Loading data...')
    if isinstance(path, str):
        data = np.load(path)
    else:
        data = path

    #: Fix mirroring in data
    data = axes_swap(data)
    data = filter_batch(data)

    rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']}) # For plotting
    if rainbow_leg_plot:
        #: Plotting the first frame with different colors for each leg to verify leg
        #  order in the data
        plot_legs_3d_rainbow(data, no_legs=no_legs, points_per_leg=points_per_leg)

    #: Create a name vector from the input data to ease data management
    name_vector, extended_name_vector = get_name_vector(joint_names=joint_names,
                                                        coxa_names=coxa_names,
                                                        side_names=side_names,
                                                        leg_names=leg_names,
                                                        last_point_name=last_point_name)
    if len(name_vector) != data.shape[1]:
        print('WARING: the name length does not match the data length')

    #: Moving the thorax-coxa points to their median over the whole experiment
    if fix_body_coxa:
        print('Fixing body-coxa point...')
        #: Fixing the coxa position to the mean position over time
        data = fix_coxa(data, segment=0, no_legs=no_legs, points_per_leg=points_per_leg)
        #: Find the fly frame
        R_fl = find_fly_R(data, gravity_reference, forward_reference)

    #: Initializing the output dataframe
    datamat_j = pd.DataFrame(np.zeros((len(data)+1, len(extended_name_vector))),
                             columns=extended_name_vector)

    if segment_length_filename is not None:
        #: Estimate leg segment length over the whole experiment for each leg
        segment_lengths = estimate_segment_length(data, no_legs=no_legs,
                                                  points_per_leg=points_per_leg)

    if plane_dist_filename is not None:
        #: Calculate the distance of each point to the leg plane
        plane_dist = np.zeros([len(data), no_legs, points_per_leg])

    if plane_angle_name is not None:
        #: Calculate the angles between the leg sub-planes
        plane_ang = np.zeros([len(data), no_legs, 3])

    #: To calculate the error
    if error_name is not None:
        verify = True
    if verify:
        error = np.zeros((len(data), data.shape[1]))

    #: Calculating the joint angles
    print('Calculating joint angles...')
    for t in range(len(data)):
        if t%10 == 0 or t == len(data)-1:
            print('t = ' + str(t))

        for joint in range(len(name_vector)):

            #: 3DOF joint
            if joint_names[0] in name_vector[joint]:
                #: Dealing with the 3DOF joint
                coxa_name_vector = []
                for coxa_name in coxa_names:
                    coxa_name_vector = np.append(coxa_name_vector,
                                                 name_vector[joint] + '_' + coxa_name)

                #: Calculate the coxa frame
                p1 = data[t, joint, :] # The current joint point
                p2 = data[t, joint + 1, :] # The pointa after the current joint (coxa-femur)

                if len(np.array(coxa_x_ref).shape) == 0: # p3 decides the direction
                                                         # of the rotation around the coxa
                    p3 = data[t, joint + coxa_x_ref, :]
                else:
                    p3 = np.mean(data[t, joint + np.array(coxa_x_ref), :], axis=0)
                v1 = p1 - p2 # Define a vector along the coxa (segment after current joint)
                v2 = p3 - p2 # Define a vector along the leg.
                             # This is to define the rotation around the coxa.

                R_cl = find_rotation_matrix(v1, v2) # The coxa frame to lab frame matrix

                #: Calculate the fly frame (if thorax-coxa frame was not fixed)
                if not fix_body_coxa:
                    R_fl = find_fly_R(data[t, :, :], g_ref=gravity_reference,
                                      f_ref=forward_reference)

                #: Save distance from leg plane to each point
                if plane_dist_filename is not None:
                    for i in range(points_per_leg):
                        plane_dist[t, joint/points_per_leg, i] = find_dist(data[t, joint + i, :],
                                                                           R_cl[:, 2])

                #: Plotting
                if time_points_to_verify is not None:
                    if t in time_points_to_verify:
                        verify_plot = True
                    else:
                        verify_plot = False

                if verify or verify_plot or fly_frame_plot:
                    if joint <= data.shape[1]/2:
                        R_f = R_fl[0]
                    else:
                        R_f = R_fl[1]

                if coxa_frame_plot:
                    plot_coxa_frame(R_cl, data, joint, p2=p2, t=t, projection=coxa_frame_projection,
                                    points_per_leg=points_per_leg, figsize=(5, 5))
                    if not verify_plot:
                        plt.show()

                if fly_frame_plot and joint%(points_per_leg*no_legs/2) == 0:
                    plot_fly_frame(R_f, data, joint, t=t, points_per_leg=points_per_leg,
                                   no_legs=no_legs, projection=fly_frame_projection, figsize=(5, 5))
                    if not verify_plot:
                        plt.show()

                if leg_plane_plot and joint == 0:
                    for axes in [[0, 1], [0, 2], [1, 2]]:
                        plot_leg_planes(data, t, axes=axes, points_per_leg=points_per_leg,
                                        figsize=(10, 5))
                        if not verify_plot:
                            plt.show()

                #: Calculating the Euler angle from the rotation matrix
                if forward_reference == 'HMcoxa':
                    if joint > data.shape[1]/2:
                        R = np.dot(R_cl, np.linalg.inv(R_fl[1]))
                    else:
                        R = np.dot(R_cl, np.linalg.inv(R_fl[0]))
                else:
                    R = np.dot(R_cl, np.linalg.inv(R_fl)) # The coxa frame to fly frame matrix
                eul = T.euler_from_matrix(R, convention)

                #: Plotting/calculating position error
                if verify or verify_plot:
                    point_y = -R_f[:, 1] # Rotate the -y axis of the fly frame
                    point_x = R_f[:, 0] # Rotate the x axis of the fly frame

                    if verify_plot:
                        if joint == 0:
                            fig1 = plt.figure()
                            ax1 = fig1.add_subplot(111, projection='3d')
                            colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

                            ax1.plot([0], [0], [0], '--', color=colors[6]) # For legend
                            ax1.plot([0], [0], [0], colors[0]) # For legend

                            ax1.set_xlabel('x (mm)')
                            ax1.set_ylabel('y (mm)')
                            ax1.set_zlabel('z (mm)')

                    y_rot = np.dot(R, point_y)
                    yr = y_rot*norm(p1-p2)
                    point_old = yr + p1 # Keep the current point for calculating the position of the
                                        # next point

                    if verify_plot:
                        # Plot the coxa
                        x = [0, yr[0]] + p1[0]
                        y = [0, yr[1]] + p1[1]
                        z = [0, yr[2]] + p1[2]
                        ax1.plot(x, y, z, colors[0])

                    if verify:
                        # Calculate the error of the coxa-femur joint
                        error[t, joint] = norm(p2-(yr+p1))


                #: Finalizing Euler angles for Webots and save to datamat_j
                eul = np.array([eul[eul_coxa_order[0]], eul[eul_coxa_order[1]],
                                eul[eul_coxa_order[2]]]) # reordering the euler angles according
                                                         # to the order in coxa_names
                count = 0
                for coxa_name in coxa_name_vector:
                    datamat_j[coxa_name][t+1] = eul[count]
                    count += 1

            #: 1DOF joint
            elif not last_point_name in name_vector[joint]:
                p1 = data[t, joint - 1, :]
                p2 = data[t, joint, :]
                p3 = data[t, joint + 1, :]

                #: Calculate vectors of the segments around the joint (pointing away from the joint)
                v1 = np.array(p1 - p2)
                v2 = np.array(p3 - p2)

                eul = angle_between(v1, v2)

                #: Allowing angles to be negative
                if (negative_angles and name_vector[joint][3:] in negative_joints or
                        name_vector[joint][1:] in negative_joints or
                        name_vector[joint] in negative_joints):
                    x_c = np.dot(np.linalg.inv(R_cl), v2)[0]
                    if x_c < 0:
                        eul = - eul

                #: Plotting/calculating position error
                if (verify or verify_plot):
                    #: Calculate the rotation matrix for reconstruction of the current joint
                    if joint%points_per_leg == 1:
                        R_fe = T.rotation_matrix(np.pi/2 - eul, R_f[:, 2])[:3, :3]
                        R_v = R_fe
                    elif joint%points_per_leg == 2:
                        R_ti = T.rotation_matrix(-(np.pi - eul), R_f[:, 2])[:3, :3]
                        R_v = np.dot(R_ti, R_fe)
                    elif joint%points_per_leg == 3:
                        R_ta = T.rotation_matrix(np.pi - eul, R_f[:, 2])[:3, :3]
                        R_v = np.dot(R_ta, R_v)
                    point = np.dot(R_v, point_x)
                    point = point/norm(point)*norm(p2-p3)
                    p_rot = np.dot(R, point)
                    p_rot = p_rot/norm(p_rot)*norm(p2-p3)

                    #: Compute error
                    if verify:
                        error[t, joint] = norm(p3 - (p_rot + point_old))

                    #: Plot the reconstruction and the data
                    if verify_plot:
                        # Reconstruction
                        x = [0, p_rot[0]] + point_old[0]
                        y = [0, p_rot[1]] + point_old[1]
                        z = [0, p_rot[2]] + point_old[2]
                        ax1.plot(x, y, z, colors[0])

                        if joint == 28:
                            # Data
                            colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
                            for i in range(no_legs):
                                data_temp = data[t, i*points_per_leg:((i+1)*points_per_leg), :]
                                x = data_temp[:, 0]
                                y = data_temp[:, 1]
                                z = data_temp[:, 2]
                                ax1.scatter(x, y, z, color=colors[6])

                            for i in range(no_legs):
                                data_temp = data[t, i*points_per_leg:((i+1)*points_per_leg), :]
                                x = data_temp[:, 0]
                                y = data_temp[:, 1]
                                z = data_temp[:, 2]
                                ax1.plot(x, y, z, '--', color=colors[6])
                            fig1.legend(['Data', 'Recreation using joint anlges'], fontsize=11)
                            plt.show()

                    point_old = p_rot + point_old # Saving current joint position to calculate
                                                  # the next joint position

                #: Final transform to joint angles and saving the angle
                eul = np.pi - eul # The joint angle is considered as 0 when the leg is straight
                datamat_j[name_vector[joint]][t+1] = eul

    #: Unwrapping
    datamat_j = np.unwrap(datamat_j.iloc[1:, :], axis=0)

    #: Correction if the file jumps by 2pi at some point
    if ld_correction is not None:
        datamat_j = correct_ld(datamat_j, ld_correction=ld_correction,
                               plot_large_difference=plot_large_difference,
                               fps=1000/time_step, extended_name_vector=extended_name_vector)

    #: Correcting if a joint's mean position is further than pi from 0
    for joint in range(datamat_j.shape[1]):
        if abs(np.mean(datamat_j[:, joint])) > np.pi:
            datamat_j[:, joint] = datamat_j[:, joint] \
                                  - np.sign(np.mean(datamat_j[:, joint]))*2*np.pi

    #: Convert to a Pandas DataFrame
    datamat_j = pd.DataFrame(datamat_j, columns=extended_name_vector)

    #: Saving
    if joint_angle_filename is not None:
        #: print(joint angles to file before interpolation
        print('Saving joint angles to file...')
        datamat_j.to_csv(joint_angle_filename, index=False)

    if plane_dist_filename is not None:
        print('Pickling plane distance dictionary...')
        import pickle
        f = open(plane_dist_filename, 'w')
        pickle.dump(plane_dist, f)
        f.close()

    if segment_length_filename is not None:
        print('Saving leg segment lengths...')
        np.savetxt(segment_length_filename, segment_lengths)

    if plane_angle_name is not None:
        print('Pickling plane angles...')
        import pickle
        f = open(plane_angle_name, 'w')
        pickle.dump(plane_ang, f)
        f.close()

    if error_name is not None:
        print('Pickling error estimate...')
        import pickle
        error_names = []

        for i in range(no_legs):
            error_names = np.append(error_names,
                                    np.append(name_vector[(points_per_leg*i + 1): \
                                    (points_per_leg*i + 5)],
                                              name_vector[(points_per_leg*i)]))
        error = pd.DataFrame(error, columns=error_names)
        f = open(error_name, 'w')
        pickle.dump(error, f)
        f.close()

    #: Interpolating the joint angles for Webots
    if interpolate:
        print('Interpolating...')
        datamat_j = interpolate_time(datamat_j, time_step=time_step,
                                     webots_factor=webots_factor,
                                     webots_time_step=webots_time_step)
        if wt_get_to_position > 0:
            datamat_j = into_position(datamat_j, wt_get_to_position=wt_get_to_position,
                                      webots_time_step=webots_time_step)
    datamat_j = datamat_j.reset_index(drop=True)

    print('Done')
    return datamat_j


### Data manipulation functions
def get_name_vector(joint_names=['THORAX_COXA', 'COXA_FEMUR', 'FEMUR_TIBIA', 'TIBIA_TARSUS'],
                    coxa_names=['LD', 'T_1', 'T_2'], side_names=['L', 'R'],
                    leg_names=['F', 'M', 'H'],
                    last_point_name='CLAW'
                   ):
    '''
        This function creates a vector of names of the joints.
    '''
    name_vector = []
    extended_name_vector = []
    segment_points = np.append(joint_names, last_point_name)
    for side in side_names:
        for leg in leg_names:
            for segment_point in segment_points:
                name_vector = np.append(name_vector, side + leg + '_' + segment_point)
                if not last_point_name in segment_point:
                    if not joint_names[0] in segment_point:
                        extended_name_vector = np.append(extended_name_vector,
                                                         side + leg + '_' + segment_point)
                    else:
                        for coxa_name in coxa_names:
                            extended_name_vector = np.append(extended_name_vector,
                                                             side + leg + '_' + segment_point
                                                             + '_' + coxa_name)

    return name_vector, extended_name_vector

def axes_swap(points3d):
    '''
        Moving the points to be approximately around zero, swapping and mirroring some axes
    '''
    points3d -= np.median(points3d.reshape(-1, 3), axis=0)
    pts_t = points3d.copy()
    pts_t[:, :, 1] *= -1
    return pts_t

def correct_ld(datamat_j, ld_correction=np.pi/2, fps=100, plot_large_difference=False,
               extended_name_vector=None):
    '''
        This function goes through the data and corrects when an angle jumps by 2pi during the
        course of a file.
    '''

    for joint in range(datamat_j.shape[1]):
        if plot_large_difference:
            t = np.arange(0, len(datamat_j)/float(fps), 1./fps)
            plt.figure()
            plt.plot(t, datamat_j[:, joint]*180/np.pi)
            change = False

        resolved = False
        count = 0
        while not resolved:
            data_temp = np.diff(datamat_j[:, joint])
            big_diff = abs(data_temp) > ld_correction # Find big jumps in the angles
            a = np.arange(0, len(data_temp), 1)
            idx = a[big_diff]

            if count == 0 and plot_large_difference:
                plt.plot(t[idx], datamat_j[idx, joint], '*')
                count += 1

            if idx.size > 0:
                # Find the mean before the first big jump
                mean_before_first = np.mean(datamat_j[1:idx[0], joint])
                # Find the points that are too far away from that point
                too_far_away = abs(datamat_j[:, joint] - mean_before_first) > ld_correction*2
                # Correct them by 2pi if there are more than one outlier
                if sum(too_far_away) > 1:
                    datamat_j[too_far_away, joint] = datamat_j[too_far_away, joint] \
                                                     - 2*np.pi*np.sign(datamat_j[too_far_away,
                                                                                 joint])
                    if plot_large_difference:
                        change = True
                else:
                    resolved = True
            else:
                resolved = True

        if plot_large_difference:
            if change:
                plt.plot(t, datamat_j[:, joint]*180/np.pi)
                plt.title(extended_name_vector[joint])
                plt.xlabel('Time (s)', fontsize=13)
                plt.ylabel(r'Angle ($\degree$)', fontsize=13)
                plt.legend(['Original signal', 'Points of large difference',
                            'Corrected signal'], fontsize=13)
                plt.show()
            plt.close()

    return datamat_j

def estimate_segment_length(data, no_legs=6, points_per_leg=5):
    '''
        Estimate leg segment length for each leg.
    '''
    segment_lengths = np.zeros(data.shape[1]-no_legs)
    count = 0
    for leg in range(no_legs):
        for joint in range(points_per_leg-1):
            segment = data[:, leg*points_per_leg + joint, :] \
                      - data[:, leg*points_per_leg + joint + 1, :]

            segment_norms = np.array([norm(v) for v in segment])
            segment_length = np.mean(segment_norms, axis=0)
            segment_lengths[count] = segment_length
            count += 1
    return segment_lengths

def find_median_point(data, segment, no_legs=6, points_per_leg=5):
    '''
        This function finds all the points of a particular joint (segment) and returns an array
        of their median position.
    '''
    v = np.arange(0, no_legs, 1)
    segment_points = data[:, v*points_per_leg + segment, :]
    median_points = np.median(segment_points, axis=0)
    return median_points

def fix_coxa(data, segment=0, no_legs=6, points_per_leg=5):
    # This function fixes the coxa (joint 1) position in the data.
    median_points = find_median_point(data, segment=0, no_legs=no_legs,
                                      points_per_leg=points_per_leg)
    v1 = median_points[0] - median_points[1]
    v2 = median_points[2] - median_points[1]
    a1 = angle_between(v1, v2)
    v1 = median_points[3] - median_points[4]
    v2 = median_points[5] - median_points[4]
    a2 = angle_between(v1, v2)
    v = np.arange(0, no_legs, 1)
    data[:, v*points_per_leg, :] = np.repeat(median_points[np.newaxis, :, :], len(data),
                                             axis=0)
    return data


### Functions for finding the rotation matrix in the nearest plane
def cross(a, b):
    '''
        Perform the cross product between vectors a and b.
    '''
    return [a[1]*b[2] - a[2]*b[1],
            a[2]*b[0] - a[0]*b[2],
            a[0]*b[1] - a[1]*b[0]]

def get_plane_normal(points):
    '''
    Get the normal to the best plane through "points". Points is an array of 3d points.
    '''
    fun = functools.partial(error, points=points)
    params0 = [0, 0, 0]
    res = scipy.optimize.minimize(fun, params0)

    a = res.x[0]
    b = res.x[1]
    c = res.x[2]

    normal = np.array(cross([1, 0, a], [0, 1, b]))

    return normal

def find_fly_R(data, g_ref, f_ref):
    '''
    This function finds the fly coordinate system wrt the lab frame
        g_ref - gravity reference
        f_ref - fly forward reference
    '''

    if 'coxa' in f_ref or 'coxa' in g_ref:
        if len(data.shape) == 3:
            coxa_points = copy.deepcopy(data[0, np.arange(0, 30, 5), :])
        elif len(data.shape) == 2:
            coxa_points = copy.deepcopy(data[np.arange(0, 30, 5), :])

    # Calculating y (gravity) direction
    if g_ref == 'y':
        y = np.array([0, 1, 0])
    elif g_ref == 'coxa':
        ref_coxa = get_plane_normal(coxa_points)
        y = - np.array(ref_coxa)
    else:
        print('ERROR: gravity reference ' + str(g_ref) + ' unknown.')

    # Calculating x (forward) direction

    if f_ref == 'HMcoxa':
        x_temp = [coxa_points[1, :] - coxa_points[2, :], coxa_points[4, :] - coxa_points[5, :]]
        z = [np.cross(x_temp[0], y), np.cross(x_temp[1], y)]
        x = [np.cross(y, z[0]), np.cross(y, z[1])]
        y = y/norm(y)
        R = [[], []]
        for i in range(2):
            x[i] = x[i]/norm(x[i])
            z[i] = z[i]/norm(z[i])
            R[i] = np.zeros((3, 3))
            R[i][:, 0] = x[i]
            R[i][:, 1] = y
            R[i][:, 2] = z[i]

    else:

        if f_ref == 'x':
            x_temp = np.array([1, 0, 0])
        elif f_ref == 'coxa':
            x_temp = np.mean(coxa_points[[0, 3], :], axis=0) \
                     - np.mean(coxa_points[[2, 5], :], axis=0) # Front points - hind points
        else:
            print('ERROR: gravity reference ' + str(g_ref) + ' unknown.')

        # Calculating z (left to right) direction
        z = np.cross(x_temp, y) # np.cross(y, x_temp)#
        x = np.cross(y, z)#z, y)#

        x = x/norm(x)
        y = y/norm(y)
        z = z/norm(z)

        R = np.zeros((3, 3))
        R[:, 0] = x
        R[:, 1] = y
        R[:, 2] = z

    return R

def find_dist(v, n):
    '''
        Find the distance of v from the plane normal to n.
    '''
    return np.dot(v, n)/norm(n)

def find_rotation_matrix(v1, v2):
    '''
        Find the rotation matrix corresponding to a z-axis along the normal vector n, the y-axis
        along the projection of v1 to the plane defined by n and the x-axis orthogonal to both
        pointing in the direction of v2.
    '''
    y = v1
    x_temp = v2
    z = np.cross(x_temp, y)
    x = np.cross(y, z)
    if norm(x) > 0:
        x = x/norm(x)
    if norm(y) > 0:
        y = y/norm(y)
    if norm(z) > 0:
        z = z/norm(z)
    R = np.zeros([3, 3])
    R[:, 0] = x
    R[:, 1] = y
    R[:, 2] = z
    return R


### Functions for finding the 1DOF angles
def angle_between(v1, v2):
    """
        Returns the angle in radians between vectors 'v1' and 'v2'
    """
    v1_u = v1/norm(v1)
    v2_u = v2/norm(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


### Functions for getting the file ready for Webots
def interpolate_time(datamat_j, time_step=50, webots_factor=0.1, webots_time_step=2):
    '''
    This function interpolates the data to match the timestep in Webots.

    Parameters:
        time_step - time step between datapoints in real data (in ms)
        webots_factor - the factor by which Webots multiplies time
        webots_time_step - the time step in Webots (as it is inputted)
    '''
    total_time = len(datamat_j)*time_step
    wts = webots_time_step*webots_factor # The real Webots time step in ms
    new_no_points = total_time/wts
    t_new = np.arange(0, total_time, wts)
    t_old = np.arange(0, total_time, time_step)
    new_data = np.zeros([int(new_no_points), datamat_j.shape[1]])

    for i in range(datamat_j.shape[1]):
        new_data[:, i] = np.interp(t_new, t_old, datamat_j.iloc[:, i])

    datamat_j = pd.DataFrame(new_data, columns=datamat_j.columns)
    datamat_j['TIME'] = t_new

    return datamat_j

def into_position(datamat_j, wt_get_to_position=1000, webots_time_step=2):
    '''
    Duplicate the first set of angle values to give the simulation time to get into
    a normal first position before starting to move.

    Parameters:
        wt_get_to_position - the time (in Webots time) needed to get safely into position.
                             Default is 1000.
        webots_time_step - the time step in Webots (as it is inputted).

    '''

    # Number of frames to get to basic position
    no_frames = int(wt_get_to_position/webots_time_step)
    first_frame = np.array(datamat_j.iloc[1, :])
    first_frame = pd.DataFrame([first_frame], columns=datamat_j.columns)
    f_f = copy.deepcopy(first_frame)
    for i in range(no_frames-1):
        first_frame = first_frame.append(f_f)
    datamat_j = first_frame.append(datamat_j)


    return datamat_j


### Plotting functions
def plot_legs_3d_rainbow(data, no_legs=6, points_per_leg=5):
    '''
        This function plots the joint data such that each leg has its own color.
        This is to check if the orientation in the data is correct'
    '''
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for i in range(no_legs):
        data_temp = data[0, i*points_per_leg:((i+1)*points_per_leg), :]
        x = data_temp[:, 0]
        y = data_temp[:, 1]
        z = data_temp[:, 2]
        ax.scatter(x, y, z)

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    plt.gca().set_color_cycle(colors)
    for i in range(no_legs):
        data_temp = data[0, i*points_per_leg:((i+1)*points_per_leg), :]
        x = data_temp[:, 0]
        y = data_temp[:, 1]
        z = data_temp[:, 2]
        ax.plot(x, y, z)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.legend(['Right front', 'Right middle', 'Right hind', 'Left front', 'Left middle',
                'Left hind'])

    plt.show()

def plot_coxa_frame(R_cl, data, joint, t, p2, projection='3d', points_per_leg=5, figsize=(5, 5)):
    '''
        This function plots the coxa frame and the current leg.
        Parameters:
            projection - whether to plot the leg and the frame in 3d (projection = '3d')
                         [default] or 2d (projection = '2d')[produces plot from thesis]
            figsize - the size of the figure
    '''
    fig = plt.figure(figsize=figsize)
    if projection == '3d':
        ax = fig.add_subplot(111, projection='3d')

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    #: Plot the joints (scatter)
    i = joint
    data_temp = data[t, joint:(joint + points_per_leg), :]
    x = data_temp[:, 0]
    y = data_temp[:, 1]
    if projection == '2d':
        plt.scatter(x, y)
    else:
        z = data_temp[:, 2]
        ax.scatter(x, y, z)

    #: Plot the leg segments
    plt.gca().set_color_cycle(colors)
    data_temp = data[t, joint:(joint + points_per_leg), :]
    x = data_temp[:, 0]
    y = data_temp[:, 1]
    if projection == '2d':
        plt.plot(x, y)
    else:
        z = data_temp[:, 2]
        ax.plot(x, y, z)

    #: Plot the coxa frame
    for i in range(len(R_cl)):
        data_temp = R_cl[:, i]
        x = [0, data_temp[0]] + p2[0]
        y = [0, data_temp[1]] + p2[1]

        if projection == '2d':
            plt.plot(x, y)
        else:
            z = [0, data_temp[2]] + p2[2]
            plt.plot(x, y, z)

    if projection == '3d':
        #: Create cubic bounding box to simulate equal aspect ratio
        [x_min, x_max] = ax.get_xlim()
        [y_min, y_max] = ax.get_ylim()
        [z_min, z_max] = ax.get_zlim()
        max_range = np.array([x_max-x_min, y_max-y_min, z_max-z_min]).max()
        Xb = 0.5*max_range*np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5*(x_max+x_min)
        Yb = 0.5*max_range*np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5*(y_max+y_min)
        Zb = 0.5*max_range*np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5*(z_max+z_min)

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')
        ax.set_ylim(-max_range*0.5 + 0.5*(y_max+y_min), max_range*0.5 + 0.5*(y_max+y_min))

    plt.legend(['Leg', r'$\hat{i}_{c}$', r'$\hat{j}_{c}$', r'$\hat{k}_{c}$'])
    if projection == '2d':
        plt.xlabel('x', fontsize=12)
        plt.ylabel('y', fontsize=12)
    plt.axis('equal')

def plot_fly_frame(R_f, data, joint, t, no_legs=6, points_per_leg=5, projection='2d',
                   figsize=(5, 5)):
    '''
        This function plots the fly frame and the current leg.
        Parameters:
            projection - whether to plot the leg and the frame in 3d (projection = '3d')
                         [default] or 2d (projection = '2d')[produces plot from thesis]
            figsize - the size of the figure
    '''
    fig = plt.figure(figsize=figsize)

    if projection == '3d':
        ax = fig.add_subplot(111, projection='3d')

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
    else:
        ax = fig.add_subplot(111)

    #: Finding which legs to plot (only the current side)
    if joint < points_per_leg*3:
        hl_tc_joint = 2*points_per_leg
        first_leg = 0
    else:
        hl_tc_joint = 5*points_per_leg
        first_leg = 3

    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    #: Plot the joints (scatter)
    for i in range(first_leg, no_legs/2 + first_leg):
        data_temp = data[t, i*points_per_leg:((i+1)*points_per_leg), :]
        x = data_temp[:, 0]
        z = data_temp[:, 2]
        if projection == '2d':
            plt.scatter(x, -z)
        else:
            y = data_temp[:, 1]
            ax.scatter(x, y, z)

    #: Plot the leg segments
    plt.gca().set_color_cycle(colors)
    for i in range(first_leg, no_legs/2 + first_leg):

        data_temp = data[t, i*points_per_leg:((i+1)*points_per_leg), :]
        x = data_temp[:, 0]
        z = data_temp[:, 2]
        if projection == '2d':
            plt.plot(x, -z)
        else:
            y = data_temp[:, 1]
            plt.plot(x, y, z)

    if projection == '2d':
        #: Paint the thorax-coxa points red
        data_temp = data[t, [first_leg, (first_leg+points_per_leg),
                             (first_leg+2*points_per_leg)], :]
        data_temp = data_temp[:, [0, 2]]
        plt.plot(data_temp[:, 0], -data_temp[:, 1], '*')

    #: Plot the fly frame
    for i in range(len(R_f)):
        data_temp = R_f[:, i]
        x = [0, data_temp[0]] + data[t, hl_tc_joint, 0]
        z = [0, data_temp[2]] + data[t, hl_tc_joint, 2]
        if projection == '2d':
            if i == 1:
                plt.plot(x, -z, '*', markersize=12)
            else:
                plt.plot(x, -z)
        else:
            y = [0, data_temp[1]] + data[t, hl_tc_joint, 1]
            plt.plot(x, y, z)

    if projection == '3d':
        #: Create cubic bounding box to simulate equal aspect ratio
        [x_min, x_max] = ax.get_xlim()
        [y_min, y_max] = ax.get_ylim()
        [z_min, z_max] = ax.get_zlim()
        max_range = np.array([x_max-x_min, y_max-y_min, z_max-z_min]).max()
        Xb = 0.5*max_range*np.mgrid[-1:2:2, -1:2:2, -1:2:2][0].flatten() + 0.5*(x_max+x_min)
        Yb = 0.5*max_range*np.mgrid[-1:2:2, -1:2:2, -1:2:2][1].flatten() + 0.5*(y_max+y_min)
        Zb = 0.5*max_range*np.mgrid[-1:2:2, -1:2:2, -1:2:2][2].flatten() + 0.5*(z_max+z_min)

        for xb, yb, zb in zip(Xb, Yb, Zb):
            ax.plot([xb], [yb], [zb], 'w')
        ax.set_ylim(-max_range*0.5 + 0.5*(y_max+y_min), max_range*0.5 + 0.5*(y_max+y_min))
        plt.legend(['Front leg', 'Middle leg', 'Hind leg', r'$\hat{i}_{f}$', r'$\hat{j}_{f}$',
                    r'$\hat{k}_{f}$'])
    else:
        plt.ylabel('z', fontsize=12)
        plt.xlabel('x', fontsize=12)
        plt.legend(['Front leg', 'Middle leg', 'Hind leg', 'Thorax-coxa joints',
                    r'$\hat{i}_{f}$', r'$\hat{j}_{f}$', r'$\hat{k}_{f}$'])

    plt.axis('equal')

def plot_leg_planes(data, t, axes=[0, 2], axes_names=['x', 'y', 'z'], points_per_leg=5,
                    figsize=(10, 5), fill=False):
    '''
        This function plots a schematic explaining the definition of the sub-planes of the leg.
    '''
    plt.figure(figsize=figsize)
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    i = 0

    #: Plot the joints of the first leg (scatter)
    data_temp = data[t, i*points_per_leg:((i+1)*points_per_leg), :]
    x = data_temp[:, axes[0]]
    y = data_temp[:, axes[1]]
    plt.scatter(x, y)

    plt.gca().set_color_cycle(colors)

    #: Plot the leg segments
    data_temp = data[t, i*points_per_leg:((i+1)*points_per_leg), :]
    x = data_temp[:, axes[0]]
    y = data_temp[:, axes[1]]
    plt.plot(x, y, linewidth=7)

    #: Join the joints defining each leg plane
    for i in range(2, 5):
        data_temp = data[t, [0, 1, i, 0], :]
        plt.plot(data_temp[:, axes[0]], data_temp[:, axes[1]], alpha=0.5)

    if fill:
        #: Fill the planes
        for i in range(2, 5):
            data_temp = data[t, [0, 1, i, 0], :]
            plt.gca().set_color_cycle(colors[i-1:])
            plt.fill(data_temp[:, axes[0]], data_temp[:, axes[1]])

    #: Plot the joint points defining each leg plane (scatter)
    for i in range(2, 5):
        plt.gca().set_color_cycle(colors[i-1:])
        plt.plot(data[t, i, axes[0]], data[t, i, axes[1]], 'o')

    plt.legend(['Leg', 'CFp', 'CTp', 'CCp'], fontsize=14)
    plt.xlabel(axes_names[axes[0]], fontsize=14)
    plt.ylabel(axes_names[axes[1]], fontsize=14)

def filter_batch(pts, filter_indices=None, config=None, freq=None, points_per_leg=5, no_legs=6):
    '''Smooth the positional data (from Joao Campagnolo)'''
    assert (pts.shape[-1] == 2 or pts.shape[-1] == 3)
    if filter_indices is None:
        filter_indices = np.arange(points_per_leg*no_legs)
    if config is None:
        config = {
            'freq': 100,
            'mincutoff': 0.1,
            'beta': 2.0,
            'dcutoff': 1.0
        }
    if freq is not None:
        config['freq'] = freq

    f = [[OneEuroFilter(**config) for j in range(pts.shape[-1])] for i in filter_indices]
    pts_after = np.zeros_like(pts)
    for i in range(pts.shape[0]):
        for j in range(pts.shape[1]):
            if j in filter_indices:
                pts_after[i, j, 0] = f[j][0](pts[i, j, 0], i * 0.1)
                pts_after[i, j, 1] = f[j][1](pts[i, j, 1], i * 0.1)
                pts_after[i, j, 2] = f[j][2](pts[i, j, 2], i * 0.1)
            else:
                pts_after[i, j] = pts[i, j]

    return pts_after
